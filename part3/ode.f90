!Project part 3
!Adam Durrant 00734927
!module to use RK4 or Euler time marching to solve an initial value problem
!Solves: dy/dt = RHS(y,t)
module ode
    implicit none
    real(kind=8) :: dxa
    save
contains
subroutine rk4(t0,y0,dt,nt,y)
    !4th order RK method
    implicit none
    real(kind=8), dimension(:), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt
	integer, intent (in) :: nt
    real(kind=8), dimension(size(y0)), intent(out) :: y
    real(kind=8), dimension(size(y0)) :: f1, f2, f3, f4
    real(kind=8) :: t,halfdt,fac
	integer:: k

        halfdt = 0.5d0*dt
        fac = 1.d0/6.d0

        y = y0
        t = t0

        do k = 1, nt

           f1 = dt*RHS(t, y)

           f2 = dt*RHS(t + halfdt, y + 0.5d0*f1)

           f3 = dt*RHS(t + halfdt, y + 0.5d0*f2)

           f4 = dt*RHS(t + dt, y + f3)

           y = y + (f1 + 2*f2  + 2*f3 + f4)*fac

           t = t + dt*dble(k)

        end do
end subroutine rk4
!------------------
subroutine euler(t0,y0,dt,nt,y)
    !explicit Euler method
    implicit none
    real(kind=8), dimension(:), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt
	integer, intent (in) :: nt
    real(kind=8), dimension(size(y0)), intent(out) :: y
    real(kind=8) :: t,halfdt,fac
	integer:: k

    y = y0
    t = t0
    do k = 1,nt

        y = y + dt*RHS(t,y)
        t = t + dt

    end do


end subroutine euler
!--------------------
subroutine euler_omp(t0,y0,dt,nt,numthreads,y)
    !Parallelisation: to begin with, i created a vector f which was size(y0)+2, and then indexed the matrix from 0:size(y0)+1 as this became a more easy to use index system later in the code compared to 1:size(y0)+2. I then proceed to assign y0 to the middle of f, leaving an empty element in f(0) and f(size(y0)+1) which I will use later to tackle the end points issue. At the beginning of my for loop, I assign the end points of f using the assumption that f is periodic. To maintain synchronisation, I place a barrier here as without it there were some issues when running the code. I then look at assinging sections of y depending on which thread is currently being used, and to do this I pass the same range of f through as well as an additional point on each end (to tackle the end point issue). The RHS_omp version is similar to the fd2 process, but assumes there are no end point assignments that need to be made, and then outputs a vector of length 2 less than the range of f we passed through. The range of y is assigned its new values, the time step increases, and I then add barrier to again ensure syncronsiation, before then reassinging the range of f's values to match the new range of y's values. This process then repeats nt times, producing the required answer.
    !explicit Euler method, parallelized with OpenMP
    use omp_lib
    implicit none
    real(kind=8), dimension(:), intent(in) :: y0
    real(kind=8), intent(in) :: t0,dt
	integer, intent (in) :: nt,numthreads
    real(kind=8), dimension(size(y0)), intent(out) :: y
    real(kind=8) :: t
	integer:: k,threadID,istart,iend,ntot
    real(kind=8), dimension(0:size(y0)+1) :: f

    call omp_set_num_threads(numthreads)
    
    f(1:size(f)-2) = y0
    y = y0
    t = t0
    ntot = size(y)
    !$OMP parallel private(k,istart,iend,threadID)
    threadID = omp_get_thread_num()
    call mpe_decomp1d(ntot,numthreads,threadID,istart,iend) !construct domain decomposition
    print *, 'istart,iend,threadID=',istart,iend,threadID
    do k = 1,nt
        !$OMP barrier
        f(0) = f(ntot)
        f(ntot+1) = f(1)
        
        y(istart:iend) = y(istart:iend) + dt*RHS_omp(t,f(istart-1:iend+1))
        t = t + dt

       !$OMP barrier
       f(istart:iend) = y(istart:iend)

    end do
    print *, 'finished loop:',threadID,maxval(abs(y(istart:iend))) !this will slow the code but is included for assessment.
    !$OMP end parallel

end subroutine euler_omp

!-----------------------
function RHS_omp(t,f)
    !called by euler_omp
    !RHS_omp = df/dt
    use advmodule
    implicit none
    real(kind=8), intent(in) :: t
    real(kind=8) :: invtwodx
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)-2) :: RHS_omp
    !add variable declaration for RHS_omp
    invtwodx = 0.5d0/dxa
    RHS_omp = invtwodx*(f(3:size(f))-f(1:size(f)-2))
    RHS_omp = S_adv - c_adv*RHS_omp



end function RHS_omp
!--------------------------------------

function RHS(t,f)
    !called by euler and rk4
    !RHS = df/dt
    use fdmodule
    use advmodule
    implicit none
    real(kind=8), intent(in) :: t
    real(kind=8), dimension(:), intent(in) :: f
    real(kind=8), dimension(size(f)) :: RHS
 
    call fd2(f,RHS)    
    RHS=S_adv-c_adv*RHS


end function RHS
!-----------------
end module ode


!--------------------------------------------------------------------
!  (C) 2001 by Argonne National Laboratory.
!      See COPYRIGHT in online MPE documentation.
!  This file contains a routine for producing a decomposition of a 1-d array
!  when given a number of processors.  It may be used in "direct" product
!  decomposition.  The values returned assume a "global" domain in [1:n]
!
subroutine MPE_DECOMP1D( n, numprocs, myid, s, e )
    implicit none
    integer :: n, numprocs, myid, s, e
    integer :: nlocal
    integer :: deficit

    nlocal  = n / numprocs
    s       = myid * nlocal + 1
    deficit = mod(n,numprocs)
    s       = s + min(myid,deficit)
    if (myid .lt. deficit) then
        nlocal = nlocal + 1
    endif
    e = s + nlocal - 1
    if (e .gt. n .or. myid .eq. numprocs-1) e = n

end subroutine MPE_DECOMP1D

