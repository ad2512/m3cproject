"""Adam Durrant 00734927 Project part 3, solve the 1d advection equation in fortran"""
import numpy as np
import matplotlib.pyplot as plt
import adv

#gfortran -c fdmoduleB.f90 ode.f90 advmodule.f90
#The above step should generate three .mod files
#f2py -llapack -c fdmoduleB.f90 ode.f90 advmodule.f90 -m adv --f90flags='-fopenmp' -lgomp

def advection1f(nt,tf,n,dx,c=1.0,S=0.0,display=False,numthreads=2):
    """solve advection equation, df/dt + c df/dx = S
    for x=0,dx,...,(n-1)*dx, and returns f(x,tf),fp(x,tf),
    and f4(x,tf) which are solutions obtained using the fortran
    routines ode_euler, ode_euler_omp, ode_rk4
    -    f(x,t=0) = sin(2 pi x/L), L = n*dx
    -    nt time steps are taken from 0 to tf
    -    The solutions are plotted if display is true
    """
    adv.fdmodule.n=n
    adv.fdmodule.dx=dx
    adv.advmodule.c_adv=c
    adv.advmodule.s_adv=S
    adv.ode.dxa=dx
    x = np.zeros(n)
    y = np.zeros(n)
    L = n*dx
    for i in range(n):
        x[i]= i*dx
        y[i]= np.sin(2*np.arccos(-1)*x[i]/L)  
    f = adv.ode.euler(0,y,tf/nt,nt)
    fp = adv.ode.euler_omp(0,y,tf/nt,nt,numthreads)
    f4 = adv.ode.rk4(0,y,tf/nt,nt)
    if display==True:
        plt.figure() #make new figure
        plt.plot(x,f,'b') 	
        plt.xlabel('x')
        plt.ylabel('f(x,tf)')
        plt.title('f - Adam Durrant 00734927 - f:advection1f(%g,%g,%g,%g,%g,%g,%s)'%(nt,tf,n,dx,c,S,display))
        plt.savefig('advection1f.png')
        plt.figure() #make new figure
        plt.plot(x,fp,'b') 	
        plt.xlabel('x')
        plt.ylabel('fp(x,tf)')
        plt.title('fp - Adam Durrant 00734927 - fp:advection1f(%g,%g,%g,%g,%g,%g,%s)'%(nt,tf,n,dx,c,S,display))
        plt.savefig('advection1fp.png')
        plt.figure() #make new figure
        plt.plot(x,f4,'b') 	
        plt.xlabel('x')
        plt.ylabel('f4(x,tf)')
        plt.title('f4 - Adam Durrant 00734927 - f4:advection1f(%g,%g,%g,%g,%g,%g,%s)'%(nt,tf,n,dx,c,S,display))
        plt.savefig('advection1f4.png')
    return f, fp, f4
    

 
def test_advection1f(n):
    """compute scaled L1 errors for solutions with n points 
    produced by advection1f when c=1,S=1,tf=1,nt=16000, L=1"""
    S=1
    x = np.zeros(n)
    f_exact = np.zeros(n)
    dx=1.0/n
    nt=16000
    tf=1.0
    f,fp,f4 = advection1f(nt,tf,n,dx,1,S)
    for i in range(n):
        x[i]= i*dx
        f_exact[i] = S*tf + np.sin(2*np.arccos(-1)*(x[i]-tf))  
    e = (sum(abs(f - f_exact)))/(n)
    ep = (sum(abs(fp - f_exact)))/(n)
    e4 = (sum(abs(f4 - f_exact)))/(n)
          
    return e,ep,e4 #errors from euler, euler_omp, rk4

        
"""This section is included for assessment and must be included as is in the final
file that you submit,"""
if __name__ == '__main__':
    n = 200
    nt = 320000
    tf = 1.21
    dx = 1.0/n
    f,fp,f4 = advection1f(nt,tf,n,dx,1,0,True)
    e,ep,e4 = test_advection1f(200)
    eb,epb,e4b = test_advection1f(400)
    print e,ep,e4
    print eb,epb,e4b
    print e/eb,ep/epb,e4/e4b
    plt.show()
