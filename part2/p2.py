"""Project part 2
Adam Durrant 00734927
Use gradient thresholding to process image.
Gradient is computed using grad_omp routine in fdmodule2d
f2py  --f90flags='-fopenmp' -lgomp -c fdmodule.f90 fdmodule2d.f90 -m p2 -llapack
"""
from p2 import fdmodule2d as f2d
from scipy import misc
import numpy as np
import matplotlib.pyplot as plt

def threshold(M,fac,display=False,savefig=False):
    """set all elements of M to zero where grad_amp < fac*max(grad_amp)
    1. Added an assert statement at the beginning to ensure that the M entered is of type numpy array
    2. Added an assert statement at the beginning to ensure that the M entered does not contain any negative entries
    3. Added an assert statement at the beginning to ensure that the fac entered is 0<fac=<1
    4. Added default False settings for 'display' and 'savefig'
    """

    #check input
    assert type(M) is np.ndarray, "error, M (first argument) must be a numpy array"
    assert M.all>=0, "error, M (first argument) must not contain any negative entries"
    assert 0<fac<=1, "fac (second argument) must meet the condition 0<fac=<1"

    f2d.numthreads = 2
    f2d.n1 = np.shape(M)[1]
    f2d.n2 = np.shape(M)[0]
    f2d.dx1 = 1.0
    f2d.dx2 = 1.0
    DF1 = np.zeros(shape=(np.shape(M)[0],np.shape(M)[1],np.shape(M)[2]))
    DF2 = np.zeros(shape=(np.shape(M)[0],np.shape(M)[1],np.shape(M)[2]))
    Mthresh = M
    dfmax = np.zeros(3)
    dfamp = np.zeros(shape=(np.shape(M)[0],np.shape(M)[1],np.shape(M)[2]))
    for i in range(np.shape(M)[2]):
        DF1[:,:,i],DF2[:,:,i],dfamp[:,:,i],dfmax[i] = f2d.grad_omp(M[:,:,i])
        low_values = dfamp[:,:,i]<fac*dfmax[i]
        Mthresh[:,:,i][low_values] = 0
    if display==True:
        plt.figure()
        plt.imshow(Mthresh)
        if savefig==True:
            plt.savefig('Raccoon.png')	
    
    return Mthresh, dfmax


if __name__ == '__main__':
    M=misc.face()
    print "shape(M):",np.shape(M)
    plt.figure()
    plt.imshow(M)
    plt.show() #may need to close figure for code to continue
    N,dfmax=threshold(M,0.2,True,True) #Uncomment this line 
    plt.show()
    
