#Project part 4
import numpy as np
import matplotlib.pyplot as plt
import adv2d
#gfortran -c advmodule2d.f90
#f2py -llapack -c advmodule2d.f90 fdmoduleB.f90 fdmodule2dp.f90 ode2d.f90 -m adv2d --f90flags='-fopenmp' -lgomp


def accuracy_test(nt,nx,ny,tf=1.0,c1=1.0,c2=1.0,S=1.0,numthreads=1,display=False,par=0):
    """Input 'nt' (number of time steps), 'nx' (number of rows), 'ny' (number of columns), 'tf' (stopping time), 'c1' 'c2' 'S' (advection parameters) 'numthreads' (number of threads to use in parallelsiation), 'display' (if true, plots and saves contour plot of calculations). The function uses the 4th order runga kutta scheme to numerically solve the advection equation (whilst using a second order scheme in the calculations that bottlenecks the performance) for a given stopping time. I have made the exact solution periodic as required by useing %tf. If we do not take nt to be a sufficient size (e.g nt>100), then the errors are massive, hence I have added an assert statement.
    The contour plot shows severl circles in the centre of the plot, and when we increase tf, we see this circlular density move in the diagonal direction (bottom left -> top right). If we set S=0.0, we see that many error lines display on the graph - I have countered this by setting S=1.0 as default
    Varying our value of tf causes our
    """    
    assert nt>=100, "Please enter a larger value for nt (first argument)"
    adv2d.advmodule.c1_adv=c1
    adv2d.advmodule.c2_adv=c2
    adv2d.advmodule.s_adv=S
    adv2d.fdmodule2d.n1=nx
    adv2d.fdmodule2d.n2=ny
    adv2d.fdmodule2d.numthreads=numthreads
    adv2d.ode2d.numthreads=numthreads
    adv2d.ode2d.par=par
    dx = 1.0/nx
    dy = 1.0/ny
    adv2d.fdmodule2d.dx1=dx
    adv2d.fdmodule2d.dx2=dy
    x = np.zeros(nx)
    y = np.zeros(ny)
    f = np.zeros(shape=(nx,ny))
    dt=tf/nt
    f_exact = f
    x,y=np.meshgrid(np.arange(0,nx)*dx,np.arange(0,ny)*dy)
    f = np.exp(-100.0*((x-0.5)**2+(y-0.5)**2))
    f_exact = S*tf+np.exp(-100*(((x-c1*tf)%1-0.5)**2+((y-c2*tf)%1-0.5)**2))
    f4,time = adv2d.ode2d.rk4(0.0,f,dt,nt)
    error = np.sum(abs(f4-f_exact))/(nx*ny)
    if display==True:
        plt.figure() #make new figure
        plt.contour(x,y,f4)
        plt.colorbar()	
        plt.xlabel('X')
        plt.ylabel('Y')
        plt.title('Contour plot of Calculated solution - Adam Durrant 00734927 \n accuracy_test(%g,%g,%g,%g,%g,%g,%g,%g,%g)'%(nt,nx,ny,tf,c1,c2,S,numthreads,display))
        plt.savefig('Accuracy_Test.png')
    return error, time
    


def Error_vs_N_Time():
    """ Comparing grid size with error, fixed nt=10000,c1,c2,tf,S. nx=ny=N for this example, plot of error against N
    Trivially we see that as the gridsize increases, the error decreases.
    From the log log plot, we can see that there is a linear relationship between log(n1) and log(error). We can polyfit this relationship to obtain a value for the gradient which is approximately 2. This is due to the second order fd2 subroutine that we call from rf4, capping our error reduction rate - since fd2 is second order, we would expect a gradient of 2 which is what we acheive here.
    Our second log log plot shows our Error is affected by the time taken. Now, what is quite interesting here is that one would expect that doubling the time would decrease the error by a factor of 4 (since it is second order), however when we polyfit the curve, we get a gradient of around 1, suggesting some bottlenecking at some point. As time increases, the error naturally decreases. Interestingly we see a large jump in time around N=250/300, which might be causing this change in gradient from what we would expect.
    """
    n1values = np.array([25, 50, 75, 100, 150, 200, 250, 300, 350, 400])
    n2values = np.array([25, 50, 75, 100, 150, 200, 250, 300, 350, 400])   
    error1 = np.zeros(np.size(n1values))
    time1 = np.zeros(np.size(n1values))
    for i in range(np.size(n1values)):       
        error1[i], time1[i] = accuracy_test(5000,n1values[i],n2values[i],tf=1.0,c1=1.0,c2=1.0,S=1.0)
        print "%10.10g %10.10g %10.10g"%(n1values[i],error1[i],time1[i])
    plt.figure()
    plt.loglog(n1values,error1)
    plt.xlabel('N = nx = ny')
    plt.ylabel('Error')
    plt.title('Error vs N=nx=ny - Adam Durrant 00734927 - compare()')
    plt.savefig("Error_VS_N.png")
    p1 = np.polyfit(np.log(n1values),np.log(error1),1)
    print "Gradient of log(Error) vs log(N) = %10.5g"%(p1[0])
    plt.figure()
    plt.loglog(time1,error1)
    plt.xlabel('Time')
    plt.ylabel('Error')
    plt.title('Error vs Time - Adam Durrant 00734927 - compare()')
    plt.savefig("Error_VS_Time.png")
    p2 = np.polyfit(np.log(time1),np.log(error1),1)
    print "Gradient of log(Error) vs log(Time) = %10.5g"%(p2[0])
    return p1[0],p2[0]

def Time_vs_N(nt,n2):
    """Comparing time with gridsize. Fix the values of ny and vary nx to see the impact on computational time. I've plotted a loglog plot in the function to compare the relationship between the two, and it is clear that there is a linear relationship between log(time) and log(nx) when ny remains fixed. Polyfitting this relationship leads to a gradient of approximately 1 (if we exclude small N), suggesting that doubling the value of nx whilst ny remains the same cause the time to double. This is the expected result as doubling the value of nx doubles the grid size, so there are twice as many elements to compute through, hence it takes twice as long.
    """
    n1values = np.array([25, 50, 75, 100, 150, 200, 250, 300, 350, 400])  
    error2 = np.zeros(np.size(n1values))
    time2 = np.zeros(np.size(n1values))
    for i in range(np.size(n1values)):     
        error2[i], time2[i] = accuracy_test(nt,n1values[i],n2,tf=1.0,c1=1.0,c2=1.0,S=1.0)
        print "%10.10g %10.10g %10.10g"%(n1values[i],error2[i],time2[i])
    plt.figure()
    plt.loglog(n1values,time2)
    plt.xlabel('N = nx = ny')
    plt.ylabel('Time')
    plt.title('Time vs n1 - Adam Durrant 00734927 - compare()')
    plt.savefig("Time_VS_n1.png")
    p = np.polyfit(np.log(n1values),np.log(time2),1)
    print "Gradient of log(Time) vs log(n1) = %10.5g"%(p[0])
    return p[0]

def Nt_vs_Time_Error():
    """First plot compares how varying the value of Nt effects the computational time with fixed n2 and n2 values. From the plot, we can see that there is a clear linear relationship between log(Nt) and log(time), and by polyfitting the line we see that the gradient is apporoximately one, suggesting that if you double the value of Nt, you double the computational time. This is the expected outcome, since our Nt runs through a for loop, so double Nt doubles the number of times we run our for loop.
       Second plot looks at how varying the value of Nt effects the error rate. As we can see, there is a dip in the error rate when increasing from small values of Nt, however past a certain point (roughly Nt~1000), there is no real benefit of increasing Nt any further as it does not provide a notably decrease in the error rate, despite the increased computational time. 
    """
    ntvalues = np.array([200,400,600,800,1000,2000,3000,5000,10000,15000,20000,50000])  
    error3 = np.zeros(np.size(ntvalues))
    time3 = np.zeros(np.size(ntvalues))
    for i in range(np.size(ntvalues)):     
        error3[i], time3[i] = accuracy_test(ntvalues[i],100,100,tf=1.0,c1=1.0,c2=1.0,S=1.0)
        print "%10.10g %10.10g %10.10g"%(ntvalues[i],error3[i],time3[i])
    plt.figure()
    plt.loglog(time3,ntvalues)
    plt.xlabel('Time')
    plt.ylabel('Nt')
    plt.title('Nt vs Time - Adam Durrant 00734927 - Nt_vs_Time_Error()')
    plt.savefig("Nt_vs_Time.png")
    p = np.polyfit(np.log(time3[2:np.size(ntvalues)]),np.log(ntvalues[2:np.size(ntvalues)]),1)
    print "Gradient of log(Nt) vs log(time) = %10.5g"%(p[0])
    plt.figure()
    plt.plot(ntvalues,error3)
    plt.xlabel('Nt')
    plt.ylabel('Error')
    plt.title('Nt vs Error - Adam Durrant 00734927 - Nt_vs_Time_Error()')
    plt.savefig("Nt_vs_Error.png")
    return p[0]


def speedup(numthreads):
    """
    """ 
    n1values = [25, 50, 75, 100, 150, 200, 300, 400]
    n2values = [25, 50, 75, 100, 150, 200, 300, 400]
    nn = np.size(n1values)
    speedup = np.empty((nn,nn))
    t = np.empty((nn,nn))
    e = np.empty((nn,nn))
    tp = np.empty((nn,nn))
    ep = np.empty((nn,nn))
    
    #loop through 50<n1,n2<1600 storing speedup
    for i in range(np.size(n1values)):
        for j in range(np.size(n2values)):
            print "running with n1,n2=%g %g"%(n1values[i],n2values[j])   
            e[i,j],t[i,j] = accuracy_test(1000,n1values[i],n2values[j],tf=1.0,c1=1.0,c2=1.0,S=1.0)
            ep[i,j],tp[i,j] = accuracy_test(1000,n1values[i],n2values[j],tf=1.0,c1=1.0,c2=1.0,S=1.0,numthreads=numthreads,par=1)
            speedup[i,j] = t[i,j]/tp[i,j] #compute and store speedup
    #display results
    plt.figure()
    plt.plot(n1values,speedup[:,1::2])
    plt.legend(('n2=50','100','200','400'),loc='best')
    plt.xlabel('n1')
    plt.ylabel('speedup')
    plt.title('Speedup - Adam Durrant 00734927 - speedup(%g)'%(numthreads))
    plt.axis('tight')
    plt.savefig("Speedup.png")            
    return speedup,e,t,ep,tp

#######################################
if __name__ == '__main__':
    e1,t1 = accuracy_test(1000,100,100,display=True)
    e2,t2 = accuracy_test(1000,100,100,tf=1.2,display=True)
    e3,t3 = accuracy_test(1000,100,100,tf=1.6,display=True)
    e4,t4 = accuracy_test(1000,100,100,tf=2.0,display=True)
    e5,t5 = accuracy_test(1000,100,100,display=True)
    p1,p2 = Error_vs_N_Time()
    p3 = Time_vs_N(1000,100)
    p4 = Nt_vs_Time_Error()
    speedup(2)
    plt.show()
